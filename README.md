# Hackernews ReactJS

## Stack:

1. ReactJS with antd (AntDesign)
2. Docker

### List of command

- install

`yarn install` or `yarn`

- Test suites local

`yarn test-dev`

- Docker build

`docker build -f Dockerfile.prod -t hackernews .`

- Run the docker in local machine
  `docker run -p 3000:3000 hackernews`
  or
  `docker-compose up -d`

### Description of each folders inside the src

<h4>__tests__</h4>
<p>The __tests__ folder was made for the files that will do the test suite. It contains 10 test cases. 
</p>

<h4>components</h4>
<p>components folder will contains the components of page or the containers.</p>

<h4>constants</h4>
<p>this only containes one file that contains a constants that will be used for the whole project</p>

<h4>containers</h4>
<p>inside the containers folder, it will contains the UserContainer, CommentContainer, StoryContainer. This files have the structure for the comment page, user page and story page.</p>

<h4>fixtures</h4>
<p>fixtures is the folder containes single file that will have an example of test input cases. This folder is made for the test cases.</p>

<h4>hooks</h4>
<p>hooks folder will contains the infiniteScroll that willl be used across the project.</p>

<h4>images and mappers</h4>
<p>images folder contains the image (in this case only the logo) and mappers that contains a file that filled </p>
